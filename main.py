#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import csv

from NeuralNetwork import NeuralNetwork
import numpy as np


def read_data(file_name):
    months = {'jan': np.cos(np.pi / 6), 'feb': np.cos(np.pi / 3), 'mar': np.cos(np.pi / 2),
              'apr': np.cos(2 * np.pi / 3), 'may': np.cos(5 * np.pi / 6), 'jun': np.cos(np.pi),
              'jul': np.cos(7 * np.pi / 6), 'aug': np.cos(4 * np.pi / 3), 'sep': np.cos(np.pi * 3 / 2),
              'oct': np.cos(5 * np.pi / 3), 'nov': np.cos(11 * np.pi / 6), 'dec': np.cos(np.pi * 2)}
    days = {'mon': np.cos(2 * np.pi / 7), 'tue': np.cos(4 * np.pi / 7), 'wed': np.cos(6 * np.pi / 7),
            'thu': np.cos(8 * np.pi / 7), 'fri': np.cos(10 * np.pi / 7), 'sat': np.cos(12 * np.pi / 7),
            'sun': np.cos(2 * np.pi)}

    d_input = np.array([], dtype=np.float).reshape(8, 0)
    d_output = np.array([], dtype=np.float).reshape(1, 0)

    with open(file_name, newline='') as csvfile:
        data = csv.DictReader(csvfile)
        for row in data:
            d_input = np.hstack((d_input, np.array([
                [float(row['X'])],
                [float(row['Y'])],
                [months[row['month']]],
                [days[row['day']]],
                [float(row['ISI'])],
                [float(row['DMC'])],
                [float(row['DC'])],
                [float(1)]
            ])))
            d_output = np.hstack((d_output, np.array([
                [np.log(float(row['area']) + 1)]
            ])))

    # Standardization of d_input
    for i in range(d_input.shape[0] - 1):
        s = np.std(d_input[i, :])
        mean = np.mean(d_input[i, :])
        d_input[i, :] = np.subtract(d_input[i, :], mean) / s

    # Standardization of d_output
    s = np.std(d_output)
    mean = np.mean(d_output)
    d_output = np.subtract(d_output, mean) / s

    return {'input': d_input, 'output': d_output, 's': s, 'mean': mean}


def validate(model: NeuralNetwork, x: np.ndarray, y: np.ndarray, std, mean):
    samples = x.shape[1]
    calc_y = np.add(model.feedforward(x)[-1] * std, mean)
    error = np.sum(np.square(np.subtract(np.expm1(calc_y), np.expm1(np.add(y * std, mean)))))
    return np.sqrt(error / samples)


def cross_validate(data, neurons):
    s = np.std(data['output'])
    m = np.mean(data['output'])

    # split data into 10 chunks
    split_vect = range(50, 550, 50)
    split_input = np.hsplit(data['input'], split_vect)
    split_output = np.hsplit(data['output'], split_vect)

    models = []
    model_errors = []
    # learn using 9/10 chunks
    for i in range(10):
        l_input = np.column_stack([x for k, x in enumerate(split_input) if k != i])
        l_output = np.column_stack([x for k, x in enumerate(split_output) if k != i])
        models.append(NeuralNetwork([8, neurons, 1]))
        models[i].learn(l_input, l_output)
        model_errors.append(validate(models[i], split_input[i], split_output[i], s, m))

    print("Błędy wszystkich modeli: {0}".format(model_errors))
    best_model = models[model_errors.index(min(model_errors))]

    y = np.add(best_model.feedforward(data['input'])[-1] * s, m)
    error = np.sum(np.square((np.subtract(np.exp(y), np.exp(np.add(data['output'] * s, m))))))
    print("Odchylenie standardowe: {0}".format(np.sqrt(error / data['input'].shape[1])))

    return best_model


def test(model, data):
    test_in = data['input'][:, 501:517]
    test_out = data['output'][:, 501:517]
    y = model.feedforward(test_in)[-1]
    norm_y = np.expm1(np.add(y*data['s'], data['mean']))
    norm_test_out = np.expm1(np.add(test_out*data['s'], data['mean']))
    diff = np.sum(np.absolute(np.subtract(norm_y, norm_test_out)))
    for i in range(y.shape[1]):
        print("Oczekiwano: {0}, uzyskano: {1}".format(norm_y[:, i], norm_test_out[:, i]))
    print("Blad sredni na zbiorze testowym: {0}".format(diff/16))


def main():
    data = read_data('forestfires.csv')
    model = cross_validate(data, 12)
    test(model, data)


if __name__ == '__main__':
    main()
