#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from typing import List
import numpy as np
import numpy.matlib


def sigmoid(x):
    return np.tanh(x)
    # return 1/(1+np.exp(-x))


def sigmoid_derivative(x):
    return 1 - np.square(sigmoid(x))
    # return np.exp(-x)/(1+np.exp(-x))**2


# def get_1d(arr):
#     res = np.ndarray(shape=(arr.shape[1]))
#     for it in range(arr.shape[1]):
#         res[it] = arr[0, it]
#     return res


class NeuralNetwork:

    def __init__(self, layer_sizes: List[int]):
        self.layer_sizes = layer_sizes
        self. layer_num = len(layer_sizes)
        self.weights = []
        self.previous_d_weights = None
        for i in range(self.layer_num-2):
            self.weights.append(2 / np.sqrt(layer_sizes[i]) *
                                (np.matlib.rand((layer_sizes[i+1], layer_sizes[i] + 1)) -
                                 0.5 * np.matlib.ones((layer_sizes[i+1], layer_sizes[i] + 1))))
        self.weights.append(np.matlib.zeros((layer_sizes[-1], layer_sizes[-2] + 1)))

    def learn_iteration(self, x: np.ndarray, y: np.ndarray, eta):
        if x.shape[0] != self.layer_sizes[0]:
            raise ValueError("Size of learning data input is different form size od the 1st layer.")
        if y.shape[0] != self.layer_sizes[-1]:
            raise ValueError("Size of learning data output is different from size of the last layer.")
        if y.shape[1] != x.shape[1]:
            raise ValueError("Different number of learning inputs and outputs.")

        example_num = x.shape[1]
        d_weights = [np.matlib.zeros(i.shape) for i in self.weights]
        z = self.feedforward(x)
        error = np.sum(np.square(np.subtract(z[-1], y)))
        dz = [2*(np.subtract(z[-1], y))] * (self.layer_num - 1)
        for j in range(self.layer_num-2, -1, -1):  # Back propagation
            bias_factor = np.ones([1, z[j].shape[1]])
            d_weights[j] = np.dot(dz[j], np.append(sigmoid(z[j]) if j > 0 else z[j], bias_factor, axis=0).T) / example_num
            # Derivative over 'unprocessed' sums in j-1th layer
            dz[j-1] = np.multiply(self.weights[j][:, 0:self.layer_sizes[j]].T @ dz[j], sigmoid_derivative(z[j]) if j > 0 else np.matlib.ones(shape=z[j].shape))
        for i in range(len(d_weights)):
            self.weights[i] = self.weights[i] - eta*d_weights[i]
            if self.previous_d_weights is not None:
                self.weights[i] = self.weights[i] - 0.1 * self.previous_d_weights[i]
        self.previous_d_weights = [eta * i for i in d_weights]
        return error / example_num

    def learn(self, x: np.ndarray, y: np.ndarray):
        eta = 0.7
        errors = [self.learn_iteration(x, y, eta)]
        for i in range(1000):
            eta /= 1 + i * 0.01
            errors.append(self.learn_iteration(x, y, eta))
            if abs(errors[i] - errors[i-1]) < 1e-7:
                break
        return errors

    def feedforward(self, x: np.ndarray):
        if x.shape[0] != self.layer_sizes[0]:
            raise ValueError("Size of learning data input is different form size od the 1st layer.")
        z = [x]
        for i in range(self.layer_num-1):
            bias_factor = np.ones([1, z[i].shape[1]])
            z.append(self.weights[i] @ np.append(sigmoid(z[i]) if i > 0 else z[i], bias_factor, axis=0))
        return z
